import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { ManagePostsPage } from '../manage-posts/manage-posts';
import { AboutPage } from '../about/about';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidLoad() {
  }

  about() {
    this.navCtrl.push(AboutPage);
  }

  logout() {
    this.storage.clear()
      .then(() => {
        this.navCtrl.setRoot(LoginPage);
      })
      .catch(err => {
        console.log(err);
      });
  }

  managePosts() {
    this.storage.get('user_id')
    .then(id => {
      this.navCtrl.push(ManagePostsPage, {id: id});
    })
    .catch(err => {
      console.log(err);
    });
  }

}
