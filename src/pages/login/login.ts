import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { RegisterPage } from '../register/register';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  password: string;
  error: boolean;
  errorMessage: string;
  fromRegister: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private userProvider: UserProvider, private storage: Storage, private alertCtrl: AlertController) {
    this.email = this.navParams.get('email') || 'amadden3@uncc.edu';
    this.password = this.navParams.get('password') || 'pass';
    this.fromRegister = this.navParams.get('registered') || false;
    this.error = false;
    this.errorMessage = '';
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(false);
  }

  login() {
    if (this._invalidCredentials()) {
      this.error = true;
      this.errorMessage = 'Email and Password required.'
      return;
    }

    this.userProvider.loginUser(this.email, this.password)
      .then(response => {
        let data = JSON.parse(response.data);
        if (response.status != 200) {
          this.error = true;
          this.errorMessage = 'A network error has occurred'
        } else if (!data.success) {
          this._handleResponseFailure(data);
        } else {
          this._handleResponseData(data);
        }
      })
      .catch(err => {
        this.error = true;
        this.errorMessage = err.error;
      })
  }

  register() {
    this.navCtrl.setRoot(RegisterPage);
  }

  resendVerification() {
    this._getUserId()
      .then(user_id => {
        this.userProvider.resendVerificationEmail(user_id, this.email)
          .then(response => {
            let data = JSON.parse(response.data);
            if (data.success) {
              // toast success alert
            } else {
              // toast error alert
            }
          })
          .catch(err => {
            // toast err alert
          });
      });
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  private _alertUser() {
    let alert = this.alertCtrl.create({
      title: 'Hang On!',
      subTitle: 'Are you sure you want to delete this post?',
      buttons: [
        { text: 'Okay', role: 'cancel', cssClass: 'alert-okay' },
      ],
      cssClass: 'alert-buttons'
    });
    alert.present();
  }

  private _getUserId(): Promise<any> {
    return new Promise((resolve) => {
      if(this.navParams.get('user_id') != null) {
        resolve(this.navParams.get('user_id'))
      } else {
        this._queryUserId().then(id => resolve(id))
      }
    });
  }

  private _getUserDomain(): string {
    return this.email.match('(@)(.*)(.edu)$')[2];
  }

  private _handleResponseData(data) {
    this.storage.set('token', data.token);
    this.storage.set('user_id', data.user_id);
    this.storage.set('domain', this._getUserDomain());
    this.navCtrl.setRoot(TabsPage);
  }

  private _handleResponseFailure(data) {
    this.error = true;
    this.errorMessage = data.message;
    if (data.message === 'User must be verified first') {
      this.fromRegister = true;
    }
  }

  private _invalidCredentials() {
    if (this.email == '' || this.password == '') {
      return true;
    } else {
      return false;
    }
  }

  private _queryUserId(): Promise<any> {
    return new Promise(resolve => {
      this.userProvider.getUserId(this.email)
      .then(response => {
        let data = JSON.parse(response.data);
        if (data.success) {
          resolve(data.user_id);
        }
      })
      .catch(err => {
        console.log(err);
      });
    });
  }
}
