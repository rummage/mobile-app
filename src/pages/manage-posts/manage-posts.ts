import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { CreatePostPage } from '../create-post/create-post';
import { Storage } from '@ionic/storage'

@IonicPage()
@Component({
  selector: 'page-manage-posts',
  templateUrl: 'manage-posts.html',
})
export class ManagePostsPage {

  posts: any;
  offlinePostExists: boolean;
  offlinePost: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private postProvider: PostProvider, private alertCtrl: AlertController, private storage: Storage) {
    this.loadUserPosts(navParams.get('id'));
    this.checkForOfflinePosts();
  }

  ionViewDidLoad() {
  }

  checkForOfflinePosts() {
    this.offlinePostExists = false;
    this.storage.get('offline-post')
      .then(post => {
        if (post) {
          this.offlinePostExists = true;
          this.offlinePost = [];
          this.offlinePost.push(post);
        }
      });
  }

  delete(index) {
    this.postProvider.deletePost(this.posts[index]._id)
      .then(response => {
        let data = JSON.parse(response.data);
        if (data.success) {
          this.posts.splice(index, 1);
        } else {
          // toast error
        }
      })
      .catch(err => {
        // toast error
        console.log(err);
      })
  }

  deleteOfflinePost() {
    this.storage.remove('offline-post');
    this.offlinePostExists = false;
  }

  edit(index) {
    this.navCtrl.push(CreatePostPage, { post: this.posts[index], mode: 'edit' });
  }

  invokeDialog(index) {
    let alert = this.alertCtrl.create({
      title: 'Hang On!',
      subTitle: 'Are you sure you want to delete this post?',
      buttons: [
        { text: 'Cancel', role: 'cancel', cssClass: 'alert-cancel' },
        { text: 'Delete', role: 'delete', cssClass: 'alert-delete', handler: () => { this.delete(index); } }
      ],
      cssClass: 'alert-buttons'
    });
    alert.present();
  }

  invokeOfflineDialog() {
    let alert = this.alertCtrl.create({
      title: 'Hang On!',
      subTitle: 'Are you sure you want to delete this post?',
      buttons: [
        { text: 'Cancel', role: 'cancel', cssClass: 'alert-cancel' },
        { text: 'Delete', role: 'delete', cssClass: 'alert-delete', handler: () => { this.deleteOfflinePost() } }
      ],
      cssClass: 'alert-buttons'
    });
    alert.present();
  }

  loadUserPosts(id: string) {
    this.postProvider.loadUserPosts(id)
      .then(response => {
        let data = JSON.parse(response.data);
        if (data.success) {
          this.posts = data.posts;
        } else {
          // toast error
        }
      })
      .catch(err => {
        //toast error
        console.log(err);
      });
  }

  restoreOfflinePost() {
    this.navCtrl.push(CreatePostPage, { post: this.offlinePost[0] });
  }

}
