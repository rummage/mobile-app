import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManagePostsPage } from './manage-posts';

@NgModule({
  declarations: [
    ManagePostsPage,
  ],
  imports: [
    IonicPageModule.forChild(ManagePostsPage),
  ],
})
export class ManagePostsPageModule {}
