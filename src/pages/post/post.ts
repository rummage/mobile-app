import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Keyboard } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import { MessageProvider } from '../../providers/message/message';
import { UserProvider } from '../../providers/user/user';
import { BookmarkProvider } from '../../providers/bookmark/bookmark';

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

  title: string;
  description: string;
  post_id: string;
  text: string;
  sender: string;
  messages: any;
  keyboardVisible: boolean;
  bookmarked: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private socket: Socket, private bookmarkProvider: BookmarkProvider,private messageProvider: MessageProvider, private keyboard: Keyboard, private userProvider: UserProvider) {
    this.title = navParams.get('post').title;
    this.description = navParams.get('post').description;
    this.post_id = navParams.get('post')._id;
    this.text = 'My message';
    this.keyboardVisible = false;
    this.storage.get('user_id')
      .then(sender => {
        this.sender = sender;
        this.loadMessages();
        this.checkIfBookmarked();
      });
  }

  loadMessages() {
    this.messageProvider.loadMessages(this.post_id)
      .then(response => { 
        let data = JSON.parse(response.data);
        this.messages = data.messages;
        this.socket.connect();
        this.socket.emit('subscribe', this.post_id);
        this.observeNewMessages().subscribe(data => {
          this.messages.push({ message: data.message, sender: data.sender });
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  observeNewMessages(): Observable<any> {
    let observable = new Observable(observer => {
      this.socket.on('conversation', (data) => {
        observer.next(data);
      });
    });
    return observable;
  }

  bookmarkPost() {
    this.bookmarkProvider.bookmarkPost(this.sender, this.post_id, this.title)
    .then(response => {
      if(response.succes) {
        this.bookmarked = true;
      } else {
        this.bookmarked = false;
      }
    })
    .catch(err => {
      console.log(err);
    })
  }

  checkIfBookmarked() {
    this.bookmarkProvider.queryBookmark(this.sender, this.post_id)
    .then(response => {
      let data = JSON.parse(response.data)
      if(data.success) {
        this.bookmarked = true;
      }
    })
    .catch(err => {
      console.log(err);
    })
  }

  onKeyboardShow() {
    this.keyboardVisible = true;
  }

  onKeyboardHide() {
    this.keyboardVisible = false;
  }

  sendMessage() {
    this.messageProvider.sendMessage(this.text, this.sender, this.post_id, this.title)
      .then(response => {
        let data = JSON.parse(response.data);
        if (data.response.success) {
          this.messages.push({ sender: this.sender, message: this.text });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
}
