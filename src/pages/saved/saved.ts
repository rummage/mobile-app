import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BookmarkProvider } from '../../providers/bookmark/bookmark';
import { PostPage } from '../post/post';
import { PostProvider } from '../../providers/post/post';

@IonicPage()
@Component({
  selector: 'page-saved',
  templateUrl: 'saved.html',
})
export class SavedPage {

  bookmarks: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private bookmarkProvider: BookmarkProvider, private postProvider: PostProvider) {
  }

  ionViewWillEnter() {
    this.storage.get('user_id')
    .then(id => {
      this.loadBookmarks(id);
    })
  }

  loadBookmarks(id: string) {
    this.bookmarkProvider.queryBookmarks(id)
    .then(response => {
      let data = JSON.parse(response.data);
      this.bookmarks = data.bookmarks;
    })
    .catch(err => {
      console.log(err);
    });
  }

  open(index: any) {
    this.postProvider.queryPost(this.bookmarks[index].post_id)
    .then(response => {
      let data = JSON.parse(response.data);
      this.navCtrl.push(PostPage, {post: data.post});
    })
    .catch(err => {
      console.log(err);
    });
  }

  delete(index: any) {
    this.bookmarkProvider.deleteBookmark(this.bookmarks[index]._id)
    .then(response => {
      let data = JSON.parse(response.data);
      if(data.success) {
        this.bookmarks.splice(index, 1);
      } else {
        // toast error
      }
    })
    .catch(err => {
      console.log(err);
    })
  }
}
