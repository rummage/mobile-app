import { Component } from '@angular/core';
import { NavController, IonicPage, InfiniteScroll } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { Storage } from '@ionic/storage';
import { CreatePostPage } from '../create-post/create-post';
import { PostPage } from '../post/post';
import { SettingsPage } from '../settings/settings';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: any;
  limit: number;
  domain: string;
  currentFilter: string;
  trendingFilter: boolean;
  constructor(public navCtrl: NavController, private postProvider: PostProvider, private storage: Storage) {
    this.trendingFilter = true;
    this.filterNewest();
  }

  createPost() {
    this.navCtrl.push(CreatePostPage);
  }

  //TODO: Implement this server side
  filterTrending() {
    this.trendingFilter = true;
    // this.storage.get('domain')
    // .then(domain => {
    //   this.loadPosts(domain, 'trending');
    // });
  }

  filterNewest() {
    this.trendingFilter = false;
    this.limit = 5;
    this.storage.get('domain')
      .then(domain => {
        this.domain = domain;
        this.currentFilter = 'newest';
        this.loadPosts(this.domain, this.currentFilter, String(this.limit));
      });
  }

  loadPosts(domain: string, filter: string, limit: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.postProvider.loadPosts(domain, filter, limit)
        .then(response => {
          let data = JSON.parse(response.data);
          if (!data.success) {
            // toast error
            reject(false);
          } else {
            this.posts = data.posts;
            resolve(true);
          }
        })
        .catch(err => {
          // toast error
          console.log(err);
          reject(false);
        });
    });
  }

  openDetailPost(i) {
    this.navCtrl.push(PostPage, { post: this.posts[i] });
  }

  openSettings() {
    this.navCtrl.push(SettingsPage);
  }

  queryMorePosts(infiniteScroll: InfiniteScroll) {
    if(this.limit < 30) {
      this.limit += 5;
      setTimeout(() => {
        this.loadPosts(this.domain, this.currentFilter, String(this.limit))
          .then(complete => {
            infiniteScroll.complete();
          })
          .catch(err => {
            // toast error
            infiniteScroll.complete();
          });
      }, 1000)
    } else {
      infiniteScroll.complete();
    }
  }
}
