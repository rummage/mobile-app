import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-create-post',
  templateUrl: 'create-post.html',
})
export class CreatePostPage {

  title: string;
  originalTitle: string;
  description: string;
  originalDescription: string;
  id: string;
  mode: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private postProvider: PostProvider, private alertCtrl: AlertController, private storage: Storage) {
    this.storage.get('offline-post')
      .then(offlinePost => {
        if (offlinePost) {
          this.title = offlinePost.title;
          this.description = offlinePost.description;
        } else {
          this.mode = navParams.get('mode');
          this.handleMode(this.mode);
        }
      })
      .catch(() => {
        this.mode = navParams.get('mode');
        this.handleMode(this.mode);
      });
  }

  ionViewDidLoad() {
  }

  isDirty() {
    if (this.title != this.originalTitle || this.description != this.originalDescription) {
      return true;
    } else {
      this.presentNoChangeAlert();
      return false;
    }
  }

  createPost() {
    this.postProvider.createPost(this.title, this.description)
      .then(response => {
        let data = JSON.parse(response.data);
        if (data.success) {
          // show validation -- for now we'll just dismiss the view
          this.navCtrl.pop();
        } else {
          // show error
          if (data.err.code == 'ECONNREFUSED') {
            this.presentSavePostOffline();
          }
        }
      })
      .catch(err => {
        this.presentSavePostOffline();
        console.log(err);
      });
  }

  handleMode(mode) {
    if (mode != 'edit') {
      this.title = 'This is my amazing title';
      this.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';
    } else {
      this.title = this.navParams.get('post').title;
      this.originalTitle = this.title;
      this.description = this.navParams.get('post').description;
      this.originalDescription = this.description;
      this.id = this.navParams.get('post')._id;
    }
  }

  presentNoChangeAlert() {
    let alert = this.alertCtrl.create({
      title: 'Hold Up!',
      subTitle: "Nothing changed so nothing will update (duh!).",
      buttons: [{ text: 'Okay', role: 'cancel', cssClass: 'alert-okay' }],
      cssClass: 'alert-buttons'
    });
    alert.present();
  }

  presentSavePostOffline() {
    let alert = this.alertCtrl.create({
      title: 'Aw Snap',
      subTitle: "Andrew might have broke something, but there's no reason you should suffer. Would you like to store this and try again later?",
      buttons: [
        { text: 'No', role: 'cancel', cssClass: 'alert-no' },
        { text: 'Yes', role: 'yes', cssClass: 'alert-yes', handler: () => { this.savePostOffline(); } }
      ],
      cssClass: 'alert-buttons'
    });
    alert.present();
  }


  presentUpdateAlert() {
    let alert = this.alertCtrl.create({
      title: 'Post Updated',
      subTitle: 'You have successfully updated your post.',
      buttons: [{ text: 'Okay', role: 'cancel', cssClass: 'alert-okay', handler: () => { this.navCtrl.pop() } }],
      cssClass: 'alert-buttons'
    });
    alert.present();
  }

  savePostOffline() {
    let obj = { title: this.title, description: this.description };
    this.storage.set('offline-post', obj);
  }

  updatePost() {
    if (!this.isDirty()) {
      return;
    }
    this.postProvider.updatePost(this.id, this.title, this.description, '')
      .then(response => {
        let data = JSON.parse(response.data);
        if (data.success) {
          let alert = this.alertCtrl.create({
            title: 'Post Updated',
            subTitle: 'You have successfully updated your post.',
            buttons: [{ text: 'Okay', role: 'cancel', cssClass: 'alert-okay', handler: () => { this.navCtrl.pop() } }],
            cssClass: 'alert-buttons'
          });
          alert.present();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
}
