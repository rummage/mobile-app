import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  email: string;
  password: string;
  confirmPassword: string;
  errorMessage: string;
  error: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private userProvider: UserProvider) {
    this.email = 'amadden3@uncc.edu';
    this.password = 'pass';
    this.confirmPassword = 'pass';
    this.errorMessage = '';
    this.error = false;
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(false);
  }

  cancelRegister() {
    this.navCtrl.setRoot(LoginPage);
  }

  createUser() {
    if (this.invalidCredentials()) {
      return;
    }

    this.userProvider.createUser(this.email, this.password)
      .then(response => {
        // start spinner animation
        let data = JSON.parse(response.data);
        if (response.status != 200) {
          this.error = true;
          this.errorMessage = 'A network error has occurred'
        } else if (!data.success) {
          this.error = true;
          this.errorMessage = data.message;
          return;
        } else {
          this.navCtrl.setRoot(LoginPage, { email: this.email, password: this.password, registered: true, user_id: data.user_id });
        }
      })
      .catch(err => {
        this.error = true;
        this.errorMessage = err.error;
      });
  }

  invalidCredentials(): boolean {
    if (this.email == '' || this.password == '') {
      this.error = true;
      this.errorMessage = 'Email and Password required.'
      return true;
    } else if (this.confirmPassword != this.password) {
      this.error = true;
      this.errorMessage = 'Passwords do not match.'
      return true;
    } else {
      return false;
    }
  }


  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }
}
