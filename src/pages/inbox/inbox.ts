import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { PostPage } from '../post/post';
import { PostProvider } from '../../providers/post/post';

@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {

  conversations: any;
  user_id: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private userProvider: UserProvider, private postProvider: PostProvider, private storage: Storage) {
    
  }

  ionViewWillEnter() {
    this.storage.get('user_id')
    .then(id => {
      this.user_id = id;
      this.loadConversations(id);
    });
  }

  delete(index: any) {
    let conversation = this.conversations[index];
    // this.userProvider.removeUserFromConversation(this.user_id, conversation.conversation_id)
    // .then(response => {
    //   let data = JSON.parse(response.data);
    //   if(data.success) {
    //     this.conversations.splice(index, 1);
    //   } else {
    //     // error removing message
    //   }
    // })
    // .catch(err => {
    //   console.log(err);
    // });
  }

  loadConversations(id: string) {
    // this.userProvider.loadInbox(id)
    // .then(response => {
    //   let data = JSON.parse(response.data);
    //   if(data.success) {
    //     this.conversations = data.conversations;
    //   } else {
    //     // error message
    //   }
    // })
    // .catch(err => {
    //   //err message
    //   console.log(err)
    // });
  }

  open(index: any) {
    let conversation = this.conversations[index];
    if(!conversation.private) {
      this.postProvider.queryPost(conversation.conversation_id)
      .then(response => {
        let data = JSON.parse(response.data);
        if(data.success) {
          this.navCtrl.push(PostPage, {post: data.post});
        } else {
          // error message
          console.log(response);
        }
      })
      .catch(err => {
        console.log(err);
      })
    } else {
      // open chat page
    }
  }
}
