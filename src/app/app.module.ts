import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:4007', options: {} };

import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { UserProvider } from '../providers/user/user';
import { AuthProvider } from '../providers/auth/auth';
import { SettingsPage } from '../pages/settings/settings';
import { PostPage } from '../pages/post/post';
import { InboxPage } from '../pages/inbox/inbox';
import { PostProvider } from '../providers/post/post';
import { CreatePostPage } from '../pages/create-post/create-post';
import { MessageProvider } from '../providers/message/message';
import { ManagePostsPage } from '../pages/manage-posts/manage-posts';
import { PostTitlePipe } from '../pipes/post-title/post-title';
import { AboutPage } from '../pages/about/about';
import { HomePageModule } from '../pages/home/home.module';
import { InboxPageModule } from '../pages/inbox/inbox.module';
import { ExplorePageModule } from '../pages/explore/explore.module';
import { ExplorePage } from '../pages/explore/explore';
import { SavedPage } from '../pages/saved/saved';
import { SavedPageModule } from '../pages/saved/saved.module';
import { BookmarkProvider } from '../providers/bookmark/bookmark';
import { InboxProvider } from '../providers/inbox/inbox';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    RegisterPage,
    PostPage,
    CreatePostPage,
    ManagePostsPage,
    AboutPage,
    SettingsPage,
    PostTitlePipe
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    SocketIoModule.forRoot(config),
    HomePageModule,
    SavedPageModule,
    InboxPageModule,
    ExplorePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    LoginPage,
    RegisterPage,
    SettingsPage,
    PostPage,
    InboxPage,
    CreatePostPage,
    ManagePostsPage,
    AboutPage,
    ExplorePage,
    SavedPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    AuthProvider,
    PostProvider,
    MessageProvider,
    BookmarkProvider,
    InboxProvider
  ]
})
export class AppModule {}
