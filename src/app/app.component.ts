import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private storage: Storage, private authProvider: AuthProvider) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByName('white');
      this.splashScreen.hide();
      this.storage.get('token')
        .then(token => {
          if (token) {
            this.verifyToken(token);
          } else {
            this.rootPage = LoginPage;
          }
        });
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  verifyToken(token) {
    this.authProvider.verifyToken(token)
      .then(response => {
        if (response.success) {
          this.rootPage = TabsPage;
        } else {
          this.rootPage = LoginPage;
        }
      })
      .catch(err => {
        this.rootPage = TabsPage;
      });
  }

}

