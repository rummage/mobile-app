import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';

@Injectable()
export class UserProvider {

  url: string;
  constructor(public http: HTTP) {
   this.url = 'http://localhost:4006/user';
  }

  public createUser(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.url, {email: email, password: password}, {})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  public getUserId(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/id', {}, {email: email})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  public loginUser(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/login', {}, {email: email, password: password})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  public resendVerificationEmail(user_id: string, email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/resend', {user_id: user_id, email: email}, {})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

}
