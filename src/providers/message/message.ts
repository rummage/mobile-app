import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MessageProvider {

  url: string;
  constructor(public http: HTTP) {
    this.url = 'http://localhost:4007';
  }

  loadMessages(conversation_id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/conversation/' + conversation_id, {}, {})
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  sendMessage(message: string, sender: string, conversation_id: string, receiver: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/message', { conversation_id: conversation_id, sender: sender, message: message, receiver: receiver }, {})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }
}


