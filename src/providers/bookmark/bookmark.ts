import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

@Injectable()
export class BookmarkProvider {

  url: string;
  constructor(public http: HTTP) {
    this.url = 'http://localhost:4006/bookmark';
  }

  public bookmarkPost(user_id: string, post_id: string, title: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.url + '/', {user_id: user_id, post_id: post_id, title: title}, {})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  public queryBookmark(user_id: string, post_id:string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/', {}, {user_id: user_id, post_id: post_id})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  public queryBookmarks(user_id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/bookmarks', {}, {user_id: user_id})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  public deleteBookmark(_id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.url + '/', {}, {_id: _id})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

}
