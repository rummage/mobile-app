import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthProvider {
  authUrl: string
  constructor(public http: HTTP) {
    this.authUrl = 'http://localhost:4006/auth';
  }

  public verifyToken(token): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.authUrl + '/verify', {}, { Authorization: 'Bearer ' + token })
        .then((data) => {
          let response = JSON.parse(data.data);
          resolve(response);
        })
        .catch(err => {
          reject({ success: false, err: err })
        });
    });
  }
}
