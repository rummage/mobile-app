import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class PostProvider {

  url: string;
  constructor(public http: HTTP, private storage: Storage) {
    this.url = 'http://localhost:4006/post';
  }

  createPost(title: string, description: string): Promise<any> { //eventually make this a post obj instead of any
    return new Promise((resolve, reject) => {
      Promise.all([this.storage.get('user_id'), this.storage.get('token')])
        .then(values => {
          let postObj = {
            title: title,
            description: description,
            user_id: values[0],
            access_token: values[1]
          }
          this.http.post(this.url, postObj, {})
            .then((response) => {
              resolve(response);
            })
            .catch(err => {
              reject(err);
            });
        });
    });
  }

  deletePost(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get('token')
        .then(token => {
          this.http.delete(this.url, {}, { post_id: id, access_token: token })
            .then(response => {
              resolve(response);
            })
            .catch(err => {
              reject(err);
            });
        });
    });
  }

  queryPost(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/id/' + id, {}, {})
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  loadPosts(domain: string, filter: string, limit: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/posts', {}, { domain: domain, filter: filter, limit: limit })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  loadUserPosts(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.url + '/posts', {}, { filter: 'id', id: id })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  updatePost(id: string, title: string, description: string, category: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.get('token')
        .then(token => {
          this.http.put(this.url, {post_id: id, access_token: token, title: title, description: description}, {})
            .then(response => {
              resolve(response);
            })
            .catch(err => {
              reject(err);
            });
        });
    });
  }

}
