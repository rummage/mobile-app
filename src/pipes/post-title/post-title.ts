import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'postTitle',
})
export class PostTitlePipe implements PipeTransform {

  transform(value: string, ...args) {
    if(value.length > 30) {
      return value.substr(0, 30) + '...';
    } else {
      return value;
    }
  }
}
