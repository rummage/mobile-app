import { NgModule } from '@angular/core';
import { PostTitlePipe } from './post-title/post-title';
@NgModule({
	declarations: [
		PostTitlePipe
	],
	imports: [],
	exports: [
		PostTitlePipe
	]
})
export class PipesModule {}
